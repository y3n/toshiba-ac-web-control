# ESP32 Toshiba AC control
Toshiba AC control and temperature sensor using a IR emitter and a DS18B20 digital temperature sensor

![Web Gui Preview](./printscreen.png)  


## Instructions:
 - Connect IR led to one GPIO pin (recommended pin is GPIO5)

 - Install libraries

 - Copy config.template.h to config.h

 - Flash the firmware in ESP board using 1M or 2M of SPIFFS storage.

 - Flash the data using ESP32 Sketch data upload
 
## REST API
Browser console will show the ajax calls to ESP8266 board. Running configuration can be displayed with GET request to /state path, any value can be changed with http PUT request to same path.

Ex
```
➜  ~ curl 192.168.0.71/state
{"mode":2,"fan":0,"temp":27,"power":true}%
➜  ~ curl -X PUT -d '{"temp":22}' 192.168.0.71/state
{"temp":22}%
➜  ~ curl 192.168.0.71/state
{"mode":2,"fan":0,"temp":22,"power":true}%
➜  ~ curl 192.168.0.71/sensors
{"ambientTemperature": 21.54}%
```

## DEBUG
Use mobile phone camera to see if the led is sending any IR signals when buttons are pressed. This will show if the circuit was properly made and the selected GPIO pin is the correct one.


## Credits:
Interface: https://github.com/ael-code/daikin-control  
Original project: https://github.com/mariusmotea/esp8266-AC-control
