/* Copyright 2019 Motea Marius
 * Copyright 2024 Y3N

  This example code will create a webserver that will provide basic control to AC units using the web application
  build with javascript/css. User config zone need to be updated if a different class than Collix need to be used.
  Javasctipt file may also require minor changes as in current version it will not allow to set fan speed if Auto mode
  is selected (required for Coolix).

*/
#ifndef EXAMPLES_WEB_AC_CONTROL_WEB_AC_CONTROL_H_
#define EXAMPLES_WEB_AC_CONTROL_WEB_AC_CONTROL_H_

#include <SPIFFS.h>

// Uncomment one of the following to manually override what
//    type of persistent storage is used.
// Warning: Changing filesystems will cause all previous locally
//    saved configuration data to be lost.
// #define FILESYSTEM SPIFFS
// #define FILESYSTEM LittleFS
#define FILESYSTEM SPIFFS

#if (FILESYSTEM == LittleFS)
#define FILESYSTEMSTR "LittleFS"
#else
#define FILESYSTEMSTR "SPIFFS"
#endif
#endif  // EXAMPLES_WEB_AC_CONTROL_WEB_AC_CONTROL_H_
